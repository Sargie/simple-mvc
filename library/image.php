<?php

class Image{

    public $image;
    public $newImage;
    public $tmp;
    public $name;
    public $height;
    public $width;
    public $newWidth;
    public $newHeight;
    public $savePathOriginalImage;
    public $savePathNewImage;

    public function __construct(){
    }

    public function doUpload($filename, $type = 'gallery', $h = THUMB_HEIGHT, $w = THUMB_WIDTH){

        $this->image = $filename;

        //type variable decides which sql database the image gets moved to.
        $this->checkImageSize($type);

        if($h != null && $w != null && $type='gallery'){
            $this->newHeight = $h;
            $this->newWidth = $w;
            $this->createThumbnail();
            //$this->resize();
            $this->createImageFromType($this->image['type']);
        }

        //move the original file to uploads
        move_uploaded_file($this->image['tmp_name'], $this->savePathOriginalImage);

        return $this;
    }

    public function checkImageSize($type){
        if(isset($this->image) && $this->image['tmp_name'] != null){
            if(!getimagesize($this->image['tmp_name'])){
                die("Incorrect image type!");
            }
            else{
                $name = $this->image['tmp_name'];
                if($type == 'gallery'){
                    $uploadPath = IMAGE_UPLOAD_PATH;
                }
                elseif($type == 'blog'){
                    $uploadPath = BLOG_UPLOAD_PATH;
                }

                //hash so all image names are same length
                $this->name = hash('md5', $this->image['name']);
                list($this->width, $this->height) = getimagesize($this->image['tmp_name']);
                $this->savePathOriginalImage = $uploadPath . $this->name;
            }
        }
    }

    /*This function takes a filename and height and width parameters, recreates the image to the sizes specified*/
    public function resize(){

        list($this->width, $this->height) = getimagesize($this->image['tmp_name']);

        $this->tmp = imagecreatetruecolor($this->newWidth, $this->newHeight);
    }

    public function createThumbnail(){
        //TODO Hardcoded for now. Change this though.
        $thumbWidth = 200;
        $thumbHeight = 200;
        list($this->width, $this->height) = getimagesize($this->image['tmp_name']);
        $origRatio = $this->width / $this->height;
        $thumbRatio = $thumbWidth / $thumbHeight;

        if($origRatio >= $thumbRatio){
            $newHeight = $thumbHeight;
            $newWidth = $this->width/ ($this->height / $thumbHeight);
        }
        else{
            $newWidth = $thumbWidth;
            $newHeight = $this->height / ($this->width / $thumbWidth);
        }
        $this->tmp = imagecreatetruecolor($thumbWidth, $thumbHeight);
    }

    /*This function takes an image filetype and recreates the image based on that type*/
    public function createImageFromType($type){

        if($type == 'image/gif'){
            $this->newImage = imagecreatefromgif($this->image['tmp_name']);
            imagecopyresampled($this->tmp, $this->newImage, 0, 0, 0, 0, $this->newWidth, $this->newHeight, $this->width, $this->height); 
            $this->savePathNewImage = $this->savePathOriginalImage . 'thumb';
            imagegif($this->tmp, $this->savePathNewImage, 100);
        }
        elseif($type == 'image/jpeg'){
            $this->newImage = imagecreatefromjpeg($this->image['tmp_name']);
            imagecopyresampled($this->tmp, $this->newImage, 0, 0, 0, 0, $this->newWidth, $this->newHeight, $this->width, $this->height); 
            $this->savePathNewImage = $this->savePathOriginalImage . 'thumb';
            imagegif($this->tmp, $this->savePathNewImage, 100);
        }
        elseif($type == 'image/png'){
            $this->newImage = imagecreatefrompng($this->image['tmp_name']);
            imagecopyresampled($this->tmp, $this->newImage, 0, 0, 0, 0, $this->newWidth, $this->newHeight, $this->width, $this->height); 
            $this->savePathNewImage = $this->savePathOriginalImage . 'thumb';
            imagegif($this->tmp, $this->savePathNewImage, 100);
        }
    }

    public function getName(){
        return $this->name;
    }

    public function getOriginalPath(){
        return $this->savePathOriginalImage;
    }

    public function getNewPath(){
        return $this->savePathNewImage;
    }

    public function getHeight(){
        return $this->height;
    }

    public function getWidth(){
        return $this->width;
    }
}

